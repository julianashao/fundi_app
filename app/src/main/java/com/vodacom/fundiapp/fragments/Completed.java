package com.vodacom.fundiapp.fragments;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import com.vodacom.fundiapp.R;
import com.vodacom.fundiapp.activities.ViewScheduled;
import com.vodacom.fundiapp.adapter.AlertAdapter;
import com.vodacom.fundiapp.adapter.CompletedAdapter;
import com.vodacom.fundiapp.modals.AlertsData;
import com.vodacom.fundiapp.modals.AlertsModal;
import com.vodacom.fundiapp.networking.FundFastaApi;
import com.vodacom.fundiapp.utils.FundiFastaUtils;
import com.facebook.shimmer.ShimmerFrameLayout;
import com.google.gson.Gson;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Completed extends Fragment {
    private RecyclerView recyclerView;
    private ProgressBar progressBar;
    LinearLayoutManager linearLayoutManager;
    LinearLayout layout;
    ShimmerFrameLayout shimmerFrameLayout;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = LayoutInflater.from(container.getContext()).inflate(R.layout.fragment_completed, container, false);

        shimmerFrameLayout = (ShimmerFrameLayout) view.findViewById(R.id.shimmer_view_container);
        shimmerFrameLayout.startShimmerAnimation();
        recyclerView = view.findViewById(R.id.item_list);
        linearLayoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(linearLayoutManager);
        layout=view.findViewById(R.id.no_task);
        getAlerts();

        return view;
    }

    private void getAlerts() {
        FundFastaApi api = FundiFastaUtils.getAPI();
        String authorization=FundiFastaUtils.getBuyerAuthToken(getContext());

        api.getComplated(authorization).enqueue(new Callback<AlertsData>() {
            @Override
            public void onResponse(@NonNull Call<AlertsData> call, @NonNull Response<AlertsData> response) {
                shimmerFrameLayout.stopShimmerAnimation();
                shimmerFrameLayout.setVisibility(View.GONE);
                String str=response.toString();
//                Log.d("hello",authorization);

                if (response != null) {
                    shimmerFrameLayout.stopShimmerAnimation();
                    shimmerFrameLayout.setVisibility(View.GONE);

                    Log.e("Success", new Gson().toJson(response.body()));
                    Log.e("erroie", String.valueOf(response));
                    AlertsData data = response.body();
                    final List<AlertsModal> alertsModals = data.alertsModalList;
                    CompletedAdapter alertAdapter = new CompletedAdapter(getContext(),alertsModals);
                    recyclerView.setAdapter(alertAdapter);
                    if (alertsModals.size()==0){
                        layout.setVisibility(View.VISIBLE);
                    }else {
                        layout.setVisibility(View.GONE);
                    }
//                    alertAdapter.setOnItemClickListener(new AlertAdapter().OnItemClickListener() {
//                        @Override
//                        public void onItemClick(View view, int position) {
//                            startActivity(new Intent(getContext(), ViewScheduled.class)
//                                    .putExtra("name",alertsModals.get(position).getName())
//                                    .putExtra("desc",alertsModals.get(position).getDescription())
//                                    .putExtra("title",alertsModals.get(position).getTitle())
//                            );
//                        }
//                    });


                }
            }
            @Override
            public void onFailure(Call<AlertsData> call, Throwable t) {
                Log.e("erroie", String.valueOf(t));
                shimmerFrameLayout.stopShimmerAnimation();
                shimmerFrameLayout.setVisibility(View.GONE);
            }
        });
    }
}