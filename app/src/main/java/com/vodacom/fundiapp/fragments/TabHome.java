package com.vodacom.fundiapp.fragments;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.vodacom.fundiapp.R;
import com.vodacom.fundiapp.activities.CompleteTask;
import com.vodacom.fundiapp.adapter.OngoingAlertAdapter;
import com.vodacom.fundiapp.adapter.TaskTabsAdapter;
import com.vodacom.fundiapp.modals.AlertsData;
import com.vodacom.fundiapp.modals.AlertsModal;
import com.vodacom.fundiapp.networking.FundFastaApi;
import com.vodacom.fundiapp.utils.FundiFastaUtils;
import com.google.android.material.tabs.TabLayout;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

import static android.content.Context.MODE_PRIVATE;

public class TabHome extends Fragment {
    private RecyclerView recyclerView;
    private ProgressBar progressBar;
    LinearLayoutManager linearLayoutManager;
    LinearLayout layout;
    private RadioGroup fundistatus;
    private String status;
    SharedPreferences preferences;

    RadioButton Online,Offline;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = LayoutInflater.from(container.getContext()).inflate(R.layout.fragment_tab_home, container, false);


        ViewPager pager = view.findViewById(R.id.task_methods_pager);
        TabLayout pagerTabLayout = view.findViewById(R.id.tabLayout);
        pagerTabLayout.setupWithViewPager(pager);
        TaskTabsAdapter adapter = new TaskTabsAdapter(getChildFragmentManager());
        pager.setAdapter(adapter);
        preferences = FundiFastaUtils.getSharedPreferences(getContext());

        recyclerView = view.findViewById(R.id.item_list);
        linearLayoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(linearLayoutManager);
        progressBar = view.findViewById(R.id.progressbar);
        fundistatus = view.findViewById(R.id.fundi_state);

        RadioGroup radioGroup;
        Online = (RadioButton)view.findViewById(R.id.online);
        Offline = (RadioButton)view.findViewById(R.id.offline);
        radioGroup = (RadioGroup)view.findViewById(R.id.fundi_state);

        SharedPreferences pref = getContext().getSharedPreferences("DATA", MODE_PRIVATE);
        String mystatus=preferences.getString("status", "0");
        Log.i("fundistate",mystatus);
        if (mystatus.equalsIgnoreCase("1")) {
            Online.setChecked(true);
            Offline.setChecked(false);


        }else {
            Online.setChecked(false);
            Offline.setChecked(true);
        }

        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {

                if(Online.isChecked())
                {
                    status="1";
                    updateStatus();
                }
                else {
                    status="0";
                    updateStatus();
                }


            }
        });
        getAlerts();
        return view;
    }

    private void getAlerts() {
        FundFastaApi api = FundiFastaUtils.getAPI();
        String authorization=FundiFastaUtils.getBuyerAuthToken(getContext());
//        String authorization="Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIyIiwianRpIjoiNTBiOGVhZDdhYjM1ODE2NmI1ZjZmMjcwOTdjNGQ3MWQ3YmJkYjEzMGRjNjlmOWJmMThlY2M3NzViYWEyZGUxZDAwNDg4ODg2MTNlYWI3NjgiLCJpYXQiOjE1OTU1MDk3MDIsIm5iZiI6MTU5NTUwOTcwMiwiZXhwIjoxNjI3MDQ1NzAyLCJzdWIiOiIxIiwic2NvcGVzIjpbXX0.LGhBc9KPusKBeBvVGbw_xuxSDlJ66TgGiILJt55AbgM707nNQfn77IYkq5BfwitHGteo-astMeDdooVOPEXmT7BF2TP-fjTjQWhYs8sDf6i7jou8zd4MqOv9CYM47lq_-Jg8tKhdaXb_SFXiUy8Z90MXy9RVQmP_8-EM6NL5ZH0ftFp38JODqZjvjx67W5edMZs7IpzGTAyXkg8r2aBtEqi2iecXF6MF1suouvk1C-zrxoGGwEIYrLoVzWshdNiNjJakyrtOoqGvDjVXl1OcHAXeRDaXFnr2WVNtB9fBWd4NWVKEGL9irxd4lE1FxAHIU37q7U1lH5UKc6U5RQYSfz6_3sefHf0tjT_QEjY1yAbIbiVnAYHMvr6H0cW8X33kzQRvRSV4OmsOvRA44f8DQ4ZxpNijtzB34PUkNhLNWzEOTG-VtnOGMfVqdsutCO8Epz4ON0GpxpTaV2pxeqpmINM4lIOjpi--Xr5DFSMhR08Ud6ROrX-PI9z7d0lVoWKJdY_4ib-HoK6X7DQ8UrTDjXLKFN7wMO22HW2n5auiVOsxGA5KDta9E_XdhC0fnX9MkFKzkOj6BqxWUnEuIni_ShnDIw3V07Bhjj0K4Upmvt-ihpiNt4pk8GvmSJKwce2H5X8wy3yyuG0mguBGUcKrEBzNexXUez43MRjnNHInc1I";
        api.getOngoing(authorization).enqueue(new Callback<AlertsData>() {
            @Override
            public void onResponse(@NonNull Call<AlertsData> call, @NonNull Response<AlertsData> response) {
                progressBar.setVisibility(View.GONE);
                String str=response.toString();
//                Log.d("hello",authorization);

                if (response != null) {
                    progressBar.setVisibility(View.GONE);
                    Log.e("Success", new Gson().toJson(response.body()));
                    Log.e("erroie", String.valueOf(response));
                    AlertsData data = response.body();
                    final List<AlertsModal> alertsModals = data.alertsModalList;
                    OngoingAlertAdapter alertAdapter = new OngoingAlertAdapter(getContext(),alertsModals);
                    recyclerView.setAdapter(alertAdapter);

                    alertAdapter.setOnItemClickListener(new OngoingAlertAdapter.OnItemClickListener() {
                        @Override
                        public void onItemClick(View view, int position) {
                            startActivity(new Intent(getContext(), CompleteTask.class)
                                    .putExtra("id",alertsModals.get(position).getId())
                                    .putExtra("started",alertsModals.get(position).getStarted())
                                    .putExtra("scheduled",alertsModals.get(position).getRequest_date())
                                    .putExtra("address",alertsModals.get(position).getAddress())
                            );
                        }
                    });


                }
            }
            @Override
            public void onFailure(Call<AlertsData> call, Throwable t) {
                Log.e("erroie", String.valueOf(t));
                progressBar.setVisibility(View.GONE);
            }
        });
    }


    private void updateStatus() {
                progressBar.setVisibility(View.VISIBLE);
        Retrofit retrofit = FundiFastaUtils.getRetrofit();
        FundFastaApi api = retrofit.create(FundFastaApi.class);
        String authorization=FundiFastaUtils.getBuyerAuthToken(getContext());

        api.updateStatus(authorization,status

        ).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(@NonNull Call<ResponseBody> call, @NonNull Response<ResponseBody> response) {
                Log.e("Success", new Gson().toJson(response.body()));
                Log.e("erroie", String.valueOf(response));
                if (response.isSuccessful()) {
                    LayoutInflater inflater = getLayoutInflater();

                    try {
                        JSONObject mymessage = new JSONObject(response.body().string());
                        if (mymessage.getInt("status_code")==401){

                            View layout = inflater.inflate(R.layout.custom_error_toast, null);
                            TextView message = (TextView) layout.findViewById(R.id.message);
                            message.setText(mymessage.getString("error"));
                            Toast toast = new Toast(getContext());
                            toast.setGravity(Gravity.TOP, 0, 0);
                            toast.setDuration(Toast.LENGTH_LONG);
                            toast.setView(layout);
                            toast.show();

                        }else  if (mymessage.has( "status")) {
                            SharedPreferences sp = FundiFastaUtils.getSharedPreferences(getContext());
                            sp.edit().putString("status", mymessage.getString("status")).apply();

                            status=mymessage.getString("status");
                            if (status.equalsIgnoreCase("1")) {
                                Online.setChecked(true);
                                Offline.setChecked(false);


                            }else {
                                Online.setChecked(false);
                                Offline.setChecked(true);
                            }
                        } else {
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    Log.e("Success", new Gson().toJson(response.body()));

                } else {
                    response.errorBody(); // do something with that
                }
                progressBar.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(@NonNull Call<ResponseBody> call, @NonNull Throwable t) {
                Log.d("TAG", t.toString());
                progressBar.setVisibility(View.GONE);
            }



        });
    }

}