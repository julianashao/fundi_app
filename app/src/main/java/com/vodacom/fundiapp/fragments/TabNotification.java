package com.vodacom.fundiapp.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.vodacom.fundiapp.R;
import com.vodacom.fundiapp.activities.NewRequest;
import com.vodacom.fundiapp.adapter.NotificationAdapter;
import com.vodacom.fundiapp.modals.NotificationData;
import com.vodacom.fundiapp.modals.NotificationModal;
import com.vodacom.fundiapp.networking.FundFastaApi;
import com.vodacom.fundiapp.utils.FundiFastaUtils;
import com.facebook.shimmer.ShimmerFrameLayout;
import com.google.gson.Gson;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TabNotification extends Fragment {
    private RecyclerView recyclerView;
    private ProgressBar progressBar;
    LinearLayoutManager linearLayoutManager;
    ShimmerFrameLayout shimmerFrameLayout;
    LinearLayout layout;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = LayoutInflater.from(container.getContext()).inflate(R.layout.fragment_tab_notifications, container, false);


        recyclerView = view.findViewById(R.id.item_list);
        linearLayoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(linearLayoutManager);
        progressBar = view.findViewById(R.id.progressbar);
        layout=view.findViewById(R.id.no_task);
        shimmerFrameLayout = (ShimmerFrameLayout) view.findViewById(R.id.shimmer_view_container);

        getNotification();

        return view;
    }

    private void getNotification() {
        shimmerFrameLayout.startShimmerAnimation();
        FundFastaApi api = FundiFastaUtils.getAPI();
        String authorization=FundiFastaUtils.getBuyerAuthToken(getContext());
        api.getNotification(authorization).enqueue(new Callback<NotificationData>() {
            @Override
            public void onResponse(@NonNull Call<NotificationData> call, @NonNull Response<NotificationData> response) {
                String str=response.toString();
//                Log.d("hello",authorization);


                if (response != null) {
                    Log.e("Success", new Gson().toJson(response.body()));
                    Log.e("erroie", String.valueOf(response));
                    NotificationData data = response.body();
                    final List<NotificationModal> notificationModals = data.notificationModalList;
                    NotificationAdapter notificationAdapter = new NotificationAdapter(getContext(),notificationModals);
                    recyclerView.setAdapter(notificationAdapter);

                    notificationAdapter.setOnItemClickListener(new NotificationAdapter.OnItemClickListener() {
                        @Override
                        public void onItemClick(View view, int position) {

                            if (notificationModals.get(position).getAction()==1){
//
//                                startActivity(new Intent(getContext(), NewRequest.class)
//                                        .putExtra("id",notificationModals.get(position).getId())
//                                        .putExtra("request_id",notificationModals.get(position).getId())
//                                );
                            }

                        }
                    });
                    shimmerFrameLayout.stopShimmerAnimation();

                }
                shimmerFrameLayout.stopShimmerAnimation();
                shimmerFrameLayout.setVisibility(View.GONE);
            }
            @Override
            public void onFailure(Call<NotificationData> call, Throwable t) {
                Log.e("erroie", String.valueOf(t));
                shimmerFrameLayout.stopShimmerAnimation();
                shimmerFrameLayout.setVisibility(View.GONE);

            }
        });
    }
}