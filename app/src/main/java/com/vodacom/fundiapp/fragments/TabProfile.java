package com.vodacom.fundiapp.fragments;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;

import com.vodacom.fundiapp.R;
import com.vodacom.fundiapp.activities.Login;
import com.vodacom.fundiapp.utils.FundiFastaUtils;

public class TabProfile extends Fragment {
    SharedPreferences preferences;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = LayoutInflater.from(container.getContext()).inflate(R.layout.fragment_tab_profile, container, false);

        preferences = FundiFastaUtils.getSharedPreferences(getContext());
        TextView username=view.findViewById(R.id.name);
        String name=preferences.getString("username", "");
        username.setText(name);
         view.findViewById(R.id.logout).setOnClickListener(new View.OnClickListener() {
             @Override
             public void onClick(View view) {
                 LogoutDialog();
             }
         });
        return view;
    }


    private void LogoutDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle(getString(R.string.logout));
        builder.setMessage(getString(R.string.please_click_ok_to_logout));
        builder.setCancelable(true);

        builder.setNegativeButton(
                getString(R.string.cancel2),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });

        builder.setPositiveButton(
                getString(R.string.ok),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        clearUserPrefs();
                        startActivity(new Intent(getContext(), Login.class));

                    }
                });



        AlertDialog alert = builder.create();
        alert.show();
    }

    private void clearUserPrefs() {
        SharedPreferences sp = FundiFastaUtils.getSharedPreferences(getContext());
        sp.edit().clear().apply();
    }


}