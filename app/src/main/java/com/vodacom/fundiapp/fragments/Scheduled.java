package com.vodacom.fundiapp.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.vodacom.fundiapp.R;
import com.vodacom.fundiapp.activities.ViewScheduled;
import com.vodacom.fundiapp.adapter.AlertAdapter;
import com.vodacom.fundiapp.modals.AlertsData;
import com.vodacom.fundiapp.modals.AlertsModal;
import com.vodacom.fundiapp.networking.FundFastaApi;
import com.vodacom.fundiapp.utils.FundiFastaUtils;
import com.facebook.shimmer.ShimmerFrameLayout;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Scheduled extends Fragment {
    private RecyclerView recyclerView;
    private ProgressBar progressBar;
    LinearLayoutManager linearLayoutManager;
    LinearLayout layout;
    ShimmerFrameLayout shimmerFrameLayout;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = LayoutInflater.from(container.getContext()).inflate(R.layout.fragment_scheduled, container, false);
        shimmerFrameLayout = (ShimmerFrameLayout) view.findViewById(R.id.shimmer_view_container);
        shimmerFrameLayout.startShimmerAnimation();
        recyclerView = view.findViewById(R.id.item_list);
        linearLayoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(linearLayoutManager);
        progressBar = view.findViewById(R.id.progressbar);
        layout=view.findViewById(R.id.no_task);
        getAlerts();

        return view;
    }

    private void getAlerts() {
        FundFastaApi api = FundiFastaUtils.getAPI();
        String authorization=FundiFastaUtils.getBuyerAuthToken(getContext());
//        String authorization="Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIyIiwianRpIjoiNTBiOGVhZDdhYjM1ODE2NmI1ZjZmMjcwOTdjNGQ3MWQ3YmJkYjEzMGRjNjlmOWJmMThlY2M3NzViYWEyZGUxZDAwNDg4ODg2MTNlYWI3NjgiLCJpYXQiOjE1OTU1MDk3MDIsIm5iZiI6MTU5NTUwOTcwMiwiZXhwIjoxNjI3MDQ1NzAyLCJzdWIiOiIxIiwic2NvcGVzIjpbXX0.LGhBc9KPusKBeBvVGbw_xuxSDlJ66TgGiILJt55AbgM707nNQfn77IYkq5BfwitHGteo-astMeDdooVOPEXmT7BF2TP-fjTjQWhYs8sDf6i7jou8zd4MqOv9CYM47lq_-Jg8tKhdaXb_SFXiUy8Z90MXy9RVQmP_8-EM6NL5ZH0ftFp38JODqZjvjx67W5edMZs7IpzGTAyXkg8r2aBtEqi2iecXF6MF1suouvk1C-zrxoGGwEIYrLoVzWshdNiNjJakyrtOoqGvDjVXl1OcHAXeRDaXFnr2WVNtB9fBWd4NWVKEGL9irxd4lE1FxAHIU37q7U1lH5UKc6U5RQYSfz6_3sefHf0tjT_QEjY1yAbIbiVnAYHMvr6H0cW8X33kzQRvRSV4OmsOvRA44f8DQ4ZxpNijtzB34PUkNhLNWzEOTG-VtnOGMfVqdsutCO8Epz4ON0GpxpTaV2pxeqpmINM4lIOjpi--Xr5DFSMhR08Ud6ROrX-PI9z7d0lVoWKJdY_4ib-HoK6X7DQ8UrTDjXLKFN7wMO22HW2n5auiVOsxGA5KDta9E_XdhC0fnX9MkFKzkOj6BqxWUnEuIni_ShnDIw3V07Bhjj0K4Upmvt-ihpiNt4pk8GvmSJKwce2H5X8wy3yyuG0mguBGUcKrEBzNexXUez43MRjnNHInc1I";
        api.getScheduled(authorization).enqueue(new Callback<AlertsData>() {
            @Override
            public void onResponse(@NonNull Call<AlertsData> call, @NonNull Response<AlertsData> response) {
                shimmerFrameLayout.stopShimmerAnimation();
                shimmerFrameLayout.setVisibility(View.GONE);
                String str=response.toString();
//                Log.d("hello",authorization);

                if (response != null) {
                    shimmerFrameLayout.stopShimmerAnimation();
                    shimmerFrameLayout.setVisibility(View.GONE);

                    AlertsData data = response.body();
                    final List<AlertsModal> alertsModals = data.alertsModalList;
                    AlertAdapter alertAdapter = new AlertAdapter(getContext(),alertsModals);
                    recyclerView.setAdapter(alertAdapter);
                    if (alertsModals.size()==0){
                        layout.setVisibility(View.VISIBLE);
                    }else {
                        layout.setVisibility(View.GONE);
                    }
                    alertAdapter.setOnItemClickListener(new AlertAdapter.OnItemClickListener() {
                        @Override
                        public void onItemClick(View view, int position) {
                            startActivity(new Intent(getContext(), ViewScheduled.class)
                                    .putExtra("id",alertsModals.get(position).getId())
                                    .putExtra("date",alertsModals.get(position).getRequest_date())
                                    .putExtra("address",alertsModals.get(position).getAddress())
                            );
                        }
                    });


                }
            }
            @Override
            public void onFailure(Call<AlertsData> call, Throwable t) {
                Log.e("erroie", String.valueOf(t));
                shimmerFrameLayout.stopShimmerAnimation();
                shimmerFrameLayout.setVisibility(View.GONE);
            }
        });
    }
}