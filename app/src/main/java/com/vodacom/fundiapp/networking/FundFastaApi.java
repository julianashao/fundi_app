package com.vodacom.fundiapp.networking;

import com.vodacom.fundiapp.modals.AlertsData;
import com.vodacom.fundiapp.modals.DeliverableData;
import com.vodacom.fundiapp.modals.NotificationData;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface FundFastaApi {




//a***************************uthetication routes starts***************************//

    @FormUrlEncoded
    @POST("api/fundi/auth/login")
    Call<ResponseBody> userLogin(@Field("username") String email, @Field("password") String password);



    @FormUrlEncoded
    @POST("api/fundi/auth/reset")
    Call<ResponseBody> resetPassword(@Field("email") String useremail);



    //***************************authetication  end ***************************//



    //***************************Notifications routes starts***************************//


    @GET("api/provider/notification/all")
    Call<NotificationData> getNotification(@Header("Authorization") String authorization);


    @FormUrlEncoded
    @POST("api/fundi/device")
    Call<ResponseBody> firebase(@Header("Authorization") String authorization, @Field("devicetoken") String token);


//    @POST("api/provider/notification/update")
//    Call<NotificationData> updateNotification(@Header("Authorization") String authorization, @Field("status") String status,
//                                           @Field("notification_id") String request_id);


    //***************************Request routes starts***************************//

    @GET("api/provider/requests/ongoing")
    Call<AlertsData> getOngoing(@Header("Authorization") String authorization);

    @GET("api/provider/requests/scheduled")
    Call<AlertsData> getScheduled(@Header("Authorization") String authorization);

    @GET("api/provider/requests/completed")
    Call<AlertsData> getComplated(@Header("Authorization") String authorization);

    @GET("api/provider/requests/pending")
    Call<AlertsData> getPending(@Header("Authorization") String authorization);


    @FormUrlEncoded
    @POST("api/provider/status/update")
    Call<ResponseBody> updateStatus(@Header("Authorization") String authorization, @Field("status") String status);

    @FormUrlEncoded
    @POST("api/provider/requests/update")
    Call<ResponseBody> updateFundiRequest(@Header("Authorization") String authorization, @Field("status") String status,
                                          @Field("request_id") String request_id);

    @FormUrlEncoded
    @POST("api/provider/requests/update")
    Call<ResponseBody> AcceptFundiRequest(@Header("Authorization") String authorization, @Field("status") String status,
                                          @Field("request_id") String request_id,
                                          @Field("notification_id") String notification_id);



    @FormUrlEncoded
    @POST("api/provider/requests/cancel")
    Call<ResponseBody> cancelClientRequest(@Header("Authorization") String authorization,
                                     @Field("request_id") String id,
                                     @Field("comments") String comment
    );

    @FormUrlEncoded
    @POST("api/provider/deliverables/performed")
    Call<ResponseBody> markComplete(@Header("Authorization") String authorization,
                                           @Field("request_id") String id,
                                           @Field("deliverables") String comment
    );

    @FormUrlEncoded
    @POST("api/provider/rates/submit")
    Call<ResponseBody> clientRate(@Header("Authorization") String authorization,
                                    @Field("request_id") String id,
                                  @Field("rate") String rate,
                                  @Field("comment") String comment
    );


    @POST("api/provider/deliverables/all")
    Call<DeliverableData> getDeliverable(@Header("Authorization") String authorization,
                                         @Query("request_id") String request_id);





}
