package com.vodacom.fundiapp.utils;

import android.content.Context;
import android.content.SharedPreferences;

import com.vodacom.fundiapp.networking.FundFastaApi;
import com.facebook.stetho.okhttp3.StethoInterceptor;

import org.jetbrains.annotations.NotNull;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class FundiFastaUtils {

    public static SharedPreferences getSharedPreferences(@NotNull Context context) {
        return context.getSharedPreferences("tz.co.smartcodes.prefs", Context.MODE_PRIVATE);
    }

    public static String getBuyerAuthToken(Context context) {
        SharedPreferences sp = getSharedPreferences(context);
        if (sp.contains("auth_token")) {
            return sp.getString("auth_token", "");
        }
        return "";
    }

    public static Retrofit getRetrofit() {

        OkHttpClient.Builder builder = new OkHttpClient.Builder();
        builder.addNetworkInterceptor(new StethoInterceptor())
                .connectTimeout(180, TimeUnit.SECONDS)
                .readTimeout(180, TimeUnit.SECONDS)
                .writeTimeout(180, TimeUnit.SECONDS);
        OkHttpClient okHttpClient = builder.build();

        return new Retrofit.Builder()
                .baseUrl(Helper.baseURL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(okHttpClient)
                .build();
    }

    public static FundFastaApi getAPI() {

        OkHttpClient.Builder builder = new OkHttpClient.Builder();
        builder.addNetworkInterceptor(new StethoInterceptor());
        OkHttpClient okHttpClient = builder.build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Helper.baseURL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(okHttpClient)
                .build();

        return retrofit.create(FundFastaApi.class);
    }

//    public static void displayUserSignInDialog(AppCompatActivity activity) {
//        ViewGroup viewGroup = activity.findViewById(android.R.id.content);
//        View v = activity.getLayoutInflater().inflate(R.layout.dialog_user_not_logged_in, viewGroup, false);
//        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
//        builder.setView(v);
//        AlertDialog alertDialog = builder.create();
//        // alertDialog.setCancelable(false);
//        alertDialog.show();
//    }



}
