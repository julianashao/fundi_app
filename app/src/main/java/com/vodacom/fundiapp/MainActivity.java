package com.vodacom.fundiapp;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.LayoutInflater;

import com.vodacom.fundiapp.networking.FundFastaApi;
import com.vodacom.fundiapp.utils.FundiFastaUtils;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.google.gson.Gson;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Log.i("juliana2", "sd");
        FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener(MainActivity.this,new OnSuccessListener<InstanceIdResult>() {
            @Override
            public void onSuccess(InstanceIdResult instanceIdResult) {
                String token = instanceIdResult.getToken();
                Log.i("juliana", token);
                sendfirebaseToken(token);
            }
        });
    }

    private void sendfirebaseToken(String token) {
        String Authorization= FundiFastaUtils.getBuyerAuthToken(getApplicationContext());
        String device_id = Settings.Secure.getString(getContentResolver(),
                Settings.Secure.ANDROID_ID);
        Log.i("firebasetesting", "kjj");
        FundFastaApi api = FundiFastaUtils.getAPI();

        api.firebase(Authorization,token
        ).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(@NonNull Call<ResponseBody> call, @NonNull Response<ResponseBody> response) {

                Log.i("firebasetesting", String.valueOf(response.message()));
                LayoutInflater inflater = getLayoutInflater();
                Log.e("firesuccess", new Gson().toJson(response.body()));
                if (response.isSuccessful()) {
                }
            }

            @Override
            public void onFailure(@NonNull Call<ResponseBody> call, @NonNull Throwable t) {
                Log.d("TAG", t.toString());
            }
        });

    }
}