package com.vodacom.fundiapp.activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.google.gson.Gson;
import com.vodacom.fundiapp.R;
import com.vodacom.fundiapp.fragments.TabEarnings;
import com.vodacom.fundiapp.fragments.TabHome;
import com.vodacom.fundiapp.fragments.TabNotification;
import com.vodacom.fundiapp.fragments.TabProfile;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.vodacom.fundiapp.networking.FundFastaApi;
import com.vodacom.fundiapp.utils.FundiFastaUtils;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Home extends AppCompatActivity {


    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {




        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            Fragment fragment;


            switch (item.getItemId()) {


                case R.id.navigation_home: {
                    fragment = new TabHome();
                    loadFragment(fragment);
                    return true;
                }

                case R.id.navigation_notifications:
                {
                    fragment = new TabNotification();
                    loadFragment(fragment);
                    return true;

                }

                case R.id.navigation_profile:
                {
                    fragment = new TabProfile();
                    loadFragment(fragment);
                    return true;

                }
                case R.id.navigation_earning:
                {
                    fragment = new TabEarnings();
                    loadFragment(fragment);
                    return true;

                }





            }
            return false;
        }
    };

    private void loadFragment(Fragment fragment) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.fragments_container, fragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);


        FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener(Home.this,new OnSuccessListener<InstanceIdResult>() {
            @Override
            public void onSuccess(InstanceIdResult instanceIdResult) {
                String token = instanceIdResult.getToken();
                Log.i("juliana", token);
                sendfirebaseToken(token);


            }
        });

        BottomNavigationView navigation = findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        loadFragment(new TabHome());
    }

    private void sendfirebaseToken(String token) {
        String Authorization= FundiFastaUtils.getBuyerAuthToken(getApplicationContext());
        String device_id = Settings.Secure.getString(getContentResolver(),
                Settings.Secure.ANDROID_ID);
        Log.i("firebasetesting", "kjj");
        FundFastaApi api = FundiFastaUtils.getAPI();

        api.firebase(Authorization,token
        ).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(@NonNull Call<ResponseBody> call, @NonNull Response<ResponseBody> response) {

                Log.i("firebasetesting", String.valueOf(response.message()));
                LayoutInflater inflater = getLayoutInflater();
                Log.e("firesuccess", new Gson().toJson(response.body()));
                if (response.isSuccessful()) {
                }
            }

            @Override
            public void onFailure(@NonNull Call<ResponseBody> call, @NonNull Throwable t) {
                Log.d("TAG", t.toString());

            }


        });

    }
    @Override
    public void onBackPressed() {

        AlertDialog.Builder builder = new AlertDialog.Builder(Home.this);
        builder.setTitle(R.string.app_name);
        builder.setIcon(R.mipmap.ic_launcher);
        builder.setMessage(getString(R.string.do_you_want_to_exit))
                .setCancelable(false)
                .setPositiveButton(getString(R.string.yes), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        finish();
                    }
                })
                .setNegativeButton(getString(R.string.no), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();

    }
}