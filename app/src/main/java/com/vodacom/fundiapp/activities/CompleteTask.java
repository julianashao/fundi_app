package com.vodacom.fundiapp.activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.vodacom.fundiapp.R;
import com.vodacom.fundiapp.adapter.DeliverableAdapter;
import com.vodacom.fundiapp.modals.DeliverableData;
import com.vodacom.fundiapp.modals.DeliverableModal;
import com.vodacom.fundiapp.networking.FundFastaApi;
import com.vodacom.fundiapp.utils.FundiFastaUtils;
import com.facebook.shimmer.ShimmerFrameLayout;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class CompleteTask extends AppCompatActivity {
ProgressBar progressBar;
    private RecyclerView recyclerView;
    LinearLayoutManager linearLayoutManager;
    LinearLayout layout;
    ShimmerFrameLayout shimmerFrameLayout;
    TextView started,scheduled;
    ArrayList<String> userid;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_complete_task);
        progressBar=findViewById(R.id.progressbar);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arrow) {
                CompleteTask.this.onBackPressed();
            }
        });

        userid = new ArrayList<>();

        started=findViewById(R.id.started);
        scheduled=findViewById(R.id.scheduled);
        started.setText(getIntent().getStringExtra("started"));
        scheduled.setText(getIntent().getStringExtra("scheduled"));
        findViewById(R.id.start).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {

                AlertDialog.Builder builder = new AlertDialog.Builder(CompleteTask.this);
                LayoutInflater inflater = getLayoutInflater();
                View dialogLayout = inflater.inflate(R.layout.popup_deliverables,
                        null);

                final AlertDialog dialog = builder.create();
                dialog.getWindow().setSoftInputMode(
                        WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
                dialog.setView(dialogLayout, 0, 0, 0, 0);
                dialog.setCanceledOnTouchOutside(true);
                dialog.setCancelable(true);
                WindowManager.LayoutParams wlmp = dialog.getWindow()
                        .getAttributes();
                wlmp.gravity = Gravity.BOTTOM;
                shimmerFrameLayout = (ShimmerFrameLayout) dialogLayout.findViewById(R.id.shimmer_view_container);
                shimmerFrameLayout.startShimmerAnimation();
                recyclerView = dialogLayout.findViewById(R.id.item_list);
                linearLayoutManager = new LinearLayoutManager(getApplicationContext());
                recyclerView.setLayoutManager(linearLayoutManager);
                builder.setView(dialogLayout);
                getDeliverable();
                dialogLayout.findViewById(R.id.submit).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        updaterequestStatus();
//                        startActivity(new Intent(getApplicationContext(), RateUser.class));
                    }
                });

                try {
                    dialog.show();
                }catch (Exception ex){
                    ex.printStackTrace();
                }

//
//                AlertDialog.Builder builder = new AlertDialog.Builder(CompleteTask.this);
//                LayoutInflater layoutInflater = getLayoutInflater();
//
//                //this is custom dialog
//                //custom_popup_dialog contains textview only
//                View customView = layoutInflater.inflate(R.layout.popup_deliverables, null);
//                // reference the textview of custom_popup_dialog
//
//
//                builder.setView(customView);
//                builder.create();
//                builder.show();

//                updaterequestStatus();
            }
        });
    }

    private void updaterequestStatus() {
        progressBar.setVisibility(View.VISIBLE);
        Retrofit retrofit = FundiFastaUtils.getRetrofit();
        FundFastaApi api = retrofit.create(FundFastaApi.class);
        String authorization=FundiFastaUtils.getBuyerAuthToken(getApplicationContext());
        String deliverables = Arrays.toString(new ArrayList[]{userid});
        Log.i("del",deliverables);
        String request_id=getIntent().getStringExtra("id");
        api.markComplete(authorization,request_id,deliverables).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(@NonNull Call<ResponseBody> call, @NonNull Response<ResponseBody> response) {
                Log.e("Success", new Gson().toJson(response.body()));
                Log.e("erroie", String.valueOf(response));
                Log.d("Resposne*** ",response.body().toString());
                if (response.isSuccessful()) {
                    LayoutInflater inflater = getLayoutInflater();

                    try {
                        JSONObject mymessage = new JSONObject(response.body().string());
                        if (mymessage.getInt("status_code")==401){

                            View layout = inflater.inflate(R.layout.custom_error_toast, null);
                            TextView message = (TextView) layout.findViewById(R.id.message);
                            message.setText(mymessage.getString("error"));
                            Toast toast = new Toast(getApplicationContext());
                            toast.setGravity(Gravity.TOP, 0, 0);
                            toast.setDuration(Toast.LENGTH_LONG);
                            toast.setView(layout);
                            toast.show();
                        }else  if (mymessage.has( "Message")) {
                            View layout = inflater.inflate(R.layout.custom_success_toast,
                                    null);
                            TextView message = (TextView) layout.findViewById(R.id.message);
                            message.setText(mymessage.getString("Message"));
                            Toast toast = new Toast(getApplicationContext());
                            toast.setGravity(Gravity.TOP, 0, 0);
                            toast.setDuration(Toast.LENGTH_LONG);
                            toast.setView(layout);
                            toast.show();
                            startActivity(new Intent(getApplicationContext(), RateUser.class)
                                    .putExtra("request_id",request_id));
                            finish();
                        } else {
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    Log.e("Success", new Gson().toJson(response.body()));

                } else {
                    response.errorBody(); // do something with that
                }
                progressBar.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(@NonNull Call<ResponseBody> call, @NonNull Throwable t) {
                Log.d("TAG", t.toString());
                progressBar.setVisibility(View.GONE);
            }



        });
    }

    private void getDeliverable() {
        FundFastaApi api = FundiFastaUtils.getAPI();
        String authorization=FundiFastaUtils.getBuyerAuthToken(getApplicationContext());
        String request_id=getIntent().getStringExtra("id");
        api.getDeliverable(authorization,request_id).enqueue(new Callback<DeliverableData>() {
            @Override
            public void onResponse(@NonNull Call<DeliverableData> call, @NonNull Response<DeliverableData> response) {
                shimmerFrameLayout.stopShimmerAnimation();
                shimmerFrameLayout.setVisibility(View.GONE);
                String str=response.toString();
                if (response != null) {
                    shimmerFrameLayout.stopShimmerAnimation();
                    shimmerFrameLayout.setVisibility(View.GONE);
                    DeliverableData data = response.body();
                    final List<DeliverableModal> deliverableModals = data.deliverableModalList;
                    DeliverableAdapter deliverableAdapter = new DeliverableAdapter(getApplicationContext(),deliverableModals);
                    recyclerView.setAdapter(deliverableAdapter);
                    deliverableAdapter.setOnItemClickListener(new DeliverableAdapter.OnItemClickListener() {
                        @Override
                        public void onItemClick(View view, int position) {
                            deliverableModals.get(position).setSelected(!deliverableModals.get(position).isSelected());
                            deliverableAdapter.notifyItemChanged(position);


//                            attended_members.clear();
                            userid.clear();
                            StringBuilder builder = new StringBuilder();
                            for (int i = 0; i < deliverableModals.size(); i++) {
                                if (deliverableModals.get(i).isSelected()) {
                                    Log.i("nambi List", String.valueOf(deliverableModals.get(i).getName()));
//                                    attended_members.add(members.get(i));
                                    userid.add(deliverableModals.get(i).getId());
                                }
                            }

                            Log.i("userList", String.valueOf(userid));

                        }
                    });



                }
            }
            @Override
            public void onFailure(Call<DeliverableData> call, Throwable t) {
                Log.e("erroie", String.valueOf(t));
                shimmerFrameLayout.stopShimmerAnimation();
                shimmerFrameLayout.setVisibility(View.GONE);
            }
        });
    }
}