package com.vodacom.fundiapp.activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.shimmer.ShimmerFrameLayout;
import com.vodacom.fundiapp.R;
import com.vodacom.fundiapp.adapter.CancelAdapter;
import com.vodacom.fundiapp.adapter.DeliverableAdapter;
import com.vodacom.fundiapp.modals.CancelModal;
import com.vodacom.fundiapp.modals.DeliverableModal;
import com.vodacom.fundiapp.networking.FundFastaApi;
import com.vodacom.fundiapp.utils.FundiFastaUtils;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class NewRequest extends AppCompatActivity {
ProgressBar progressBar,progressBar2;
    private RecyclerView recyclerView;
    LinearLayoutManager linearLayoutManager;
    LinearLayout layout;
    ArrayList<CancelModal> cancelModals;
    String message;
    Button cancelbtn;
TextView name,service,date,time,location,rate;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_request);
        progressBar=findViewById(R.id.progressbar);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arrow) {
                NewRequest.this.onBackPressed();
            }
        });

        findViewById(R.id.start).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                updaterequestStatus();
            }
        });

        name=findViewById(R.id.name);
        name.setText(getIntent().getStringExtra("name"));

        service=findViewById(R.id.service);
        service.setText(getIntent().getStringExtra("service"));

        location=findViewById(R.id.location);
        location.setText(getIntent().getStringExtra("address"));

        date=findViewById(R.id.date);
        time=findViewById(R.id.time);
        String input = getIntent().getStringExtra("scheduled");
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        DateFormat outputformat = new SimpleDateFormat("MMM dd,yyyy");
        DateFormat outputformat2 = new SimpleDateFormat("HH:mm aa");
        Date date1 = null;
        String sdate= null;
        String stime= null;
        try{
            date1= df.parse(input);
            sdate = outputformat.format(date1);
            stime = outputformat2.format(date1);
            date.setText(sdate);
            time.setText(stime);
        }catch(ParseException pe){
            pe.printStackTrace();
        }

        findViewById(R.id.cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {

                AlertDialog.Builder builder = new AlertDialog.Builder(NewRequest.this);
                LayoutInflater inflater = getLayoutInflater();
                View dialogLayout = inflater.inflate(R.layout.popup_cancel,
                        null);

                final AlertDialog dialog = builder.create();
                dialog.getWindow().setSoftInputMode(
                        WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
                dialog.setView(dialogLayout, 0, 0, 0, 0);
                dialog.setCanceledOnTouchOutside(true);
                dialog.setCancelable(true);
                WindowManager.LayoutParams wlmp = dialog.getWindow()
                        .getAttributes();
                wlmp.gravity = Gravity.BOTTOM;
                recyclerView = dialogLayout.findViewById(R.id.item_list);
                linearLayoutManager = new LinearLayoutManager(getApplicationContext());
                cancelbtn=dialogLayout.findViewById(R.id.cancelrequest);

                recyclerView.setLayoutManager(linearLayoutManager);
                builder.setView(dialogLayout);
                cancelModals=new ArrayList<CancelModal>();
                cancelModals.add(new CancelModal( "Message number one"));
                cancelModals.add(new CancelModal( "Message number two"));
                cancelModals.add(new CancelModal( "Message number three"));
                cancelModals.add(new CancelModal( "Message number four"));
                CancelAdapter cancelAdapter = new CancelAdapter(getApplicationContext(),cancelModals);
                recyclerView.setAdapter(cancelAdapter);
                cancelAdapter.setOnItemClickListener(new CancelAdapter.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                        cancelbtn.setVisibility(View.VISIBLE);
                        message=cancelModals.get(position).getName();
                    }
                });
                progressBar2=dialogLayout.findViewById(R.id.progressbar2);
                dialogLayout.findViewById(R.id.other).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
//                        startActivity(new Intent(getApplicationContext(), CancelMessage.class)
//                                .putExtra("request_id",getIntent().getStringExtra("request_id")));
                    }
                });
                cancelbtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        cancelRequest(message);
                    }
                });


                dialog.show();

            }
        });
    }



    private void updaterequestStatus() {
        progressBar.setVisibility(View.VISIBLE);

        Retrofit retrofit = FundiFastaUtils.getRetrofit();
        FundFastaApi api = retrofit.create(FundFastaApi.class);
        String authorization=FundiFastaUtils.getBuyerAuthToken(getApplicationContext());
        String status="1";

        String notification_id=getIntent().getStringExtra("id");
        String request_id=getIntent().getStringExtra("request_id");
        api.AcceptFundiRequest(authorization,status,request_id,notification_id

        ).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(@NonNull Call<ResponseBody> call, @NonNull Response<ResponseBody> response) {
                Log.e("Success", new Gson().toJson(response.body()));
                Log.e("erroie", String.valueOf(response));
                if (response.isSuccessful()) {
                    LayoutInflater inflater = getLayoutInflater();

                    try {
                        JSONObject mymessage = new JSONObject(response.body().string());
                        if (mymessage.getInt("status_code")==401){

                            View layout = inflater.inflate(R.layout.custom_error_toast, null);
                            TextView message = (TextView) layout.findViewById(R.id.message);
                            message.setText(mymessage.getString("error"));
                            Toast toast = new Toast(getApplicationContext());
                            toast.setGravity(Gravity.TOP, 0, 0);
                            toast.setDuration(Toast.LENGTH_LONG);
                            toast.setView(layout);
                            toast.show();
                        }else  if (mymessage.has( "Message")) {
                            View layout = inflater.inflate(R.layout.custom_success_toast,
                                    null);
                            TextView message = (TextView) layout.findViewById(R.id.message);
                            message.setText(mymessage.getString("Message"));
                            Toast toast = new Toast(getApplicationContext());
                            toast.setGravity(Gravity.TOP, 0, 0);
                            toast.setDuration(Toast.LENGTH_LONG);
                            toast.setView(layout);
                            toast.show();
                            startActivity(new Intent(getApplicationContext(), Home.class));
                            finish();
                        } else {
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    Log.e("Success", new Gson().toJson(response.body()));

                } else {
                    response.errorBody(); // do something with that
                }
                progressBar.setVisibility(View.GONE);

            }

            @Override
            public void onFailure(@NonNull Call<ResponseBody> call, @NonNull Throwable t) {
                Log.d("TAG", t.toString());
                progressBar.setVisibility(View.GONE);

            }


        });
    }

    private void cancelRequest(String message) {
        progressBar2.setVisibility(View.VISIBLE);
        cancelbtn.setVisibility(View.GONE);
        Retrofit retrofit = FundiFastaUtils.getRetrofit();
        FundFastaApi api = retrofit.create(FundFastaApi.class);
        String authorization=FundiFastaUtils.getBuyerAuthToken(getApplicationContext());
        String comment=message;
        String request_id=getIntent().getStringExtra("request_id");

        api.cancelClientRequest(authorization,request_id,comment).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(@NonNull Call<ResponseBody> call, @NonNull Response<ResponseBody> response) {
                Log.e("Success", new Gson().toJson(response.body()));
                Log.e("erroie", String.valueOf(response));
                if (response.isSuccessful()) {
                    LayoutInflater inflater = getLayoutInflater();

                    try {
                        JSONObject mymessage = new JSONObject(response.body().string());
                        if (mymessage.getInt("status_code")==200){
                            View layout = inflater.inflate(R.layout.custom_success_toast,
                                    null);
                            TextView message = (TextView) layout.findViewById(R.id.message);
                            message.setText(mymessage.getString("Message"));
                            Toast toast = new Toast(getApplicationContext());
                            toast.setGravity(Gravity.TOP, 0, 0);
                            toast.setDuration(Toast.LENGTH_LONG);
                            toast.setView(layout);
                            toast.show();
                            startActivity(new Intent(getApplicationContext(), Home.class));
                            finish();
                        }else  if (mymessage.has( "Message")) {
                            View layout = inflater.inflate(R.layout.custom_error_toast, null);
                            TextView message = (TextView) layout.findViewById(R.id.message);
                            message.setText(mymessage.getString("error"));
                            Toast toast = new Toast(getApplicationContext());
                            toast.setGravity(Gravity.TOP, 0, 0);
                            toast.setDuration(Toast.LENGTH_LONG);
                            toast.setView(layout);
                            toast.show();

                        } else {
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    Log.e("Success", new Gson().toJson(response.body()));

                } else {
                    response.errorBody(); // do something with that
                }
                progressBar2.setVisibility(View.GONE);
                cancelbtn.setVisibility(View.VISIBLE);
            }

            @Override
            public void onFailure(@NonNull Call<ResponseBody> call, @NonNull Throwable t) {
                Log.d("TAG", t.toString());
                progressBar2.setVisibility(View.GONE);
                cancelbtn.setVisibility(View.VISIBLE);
            }



        });
    }
}