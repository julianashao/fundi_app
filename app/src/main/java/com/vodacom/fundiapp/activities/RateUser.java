package com.vodacom.fundiapp.activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.vodacom.fundiapp.R;
import com.vodacom.fundiapp.networking.FundFastaApi;
import com.vodacom.fundiapp.utils.FundiFastaUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class RateUser extends AppCompatActivity {
    ProgressBar progressBar2;
    EditText fundimessage;
    Button submit;
    String score;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rate_user);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arrow) {
                RateUser.this.onBackPressed();
            }
        });

        RatingBar rBar;
        rBar = (RatingBar) findViewById(R.id.rate);
        float getrating = rBar.getRating();
        rBar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {

            @Override
            public void onRatingChanged(RatingBar arg0, float rateValue, boolean arg2) {
                // TODO Auto-generated method stub
                Log.d("Rating", "your selected value is :"+rateValue);
                float getrating = rateValue;
                score= String.valueOf(getrating);
            }
        });


        progressBar2=findViewById(R.id.progressbar2);

        fundimessage = findViewById(R.id.message);
        submit = findViewById(R.id.submit);
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String message = fundimessage.getText().toString();

                if (TextUtils.isEmpty(message)) {
                    return;
                }

                rateNow(message,score);
            }
        });



    }

    private void rateNow(String message, String score) {
        progressBar2.setVisibility(View.VISIBLE);
        submit.setVisibility(View.GONE);
        Retrofit retrofit = FundiFastaUtils.getRetrofit();
        FundFastaApi api = retrofit.create(FundFastaApi.class);
        String authorization=FundiFastaUtils.getBuyerAuthToken(getApplicationContext());
        String comment=message;
        String rate=score;
        Log.i("red",getIntent().getStringExtra("request_id"));
        String request_id=getIntent().getStringExtra("request_id");

        api.clientRate(authorization,request_id,comment,rate).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(@NonNull Call<ResponseBody> call, @NonNull Response<ResponseBody> response) {
                Log.e("Success", new Gson().toJson(response.body()));
                Log.e("erroie", String.valueOf(response));
                if (response.isSuccessful()) {
                    LayoutInflater inflater = getLayoutInflater();

                    try {
                        JSONObject mymessage = new JSONObject(response.body().string());
                        if (mymessage.getInt("status_code")==200){
                            View layout = inflater.inflate(R.layout.custom_success_toast,
                                    null);
                            TextView message = (TextView) layout.findViewById(R.id.message);
                            message.setText(mymessage.getString("Message"));
                            Toast toast = new Toast(getApplicationContext());
                            toast.setGravity(Gravity.TOP, 0, 0);
                            toast.setDuration(Toast.LENGTH_LONG);
                            toast.setView(layout);
                            toast.show();
                            startActivity(new Intent(getApplicationContext(), Home.class));
                            finish();
                        }else  if (mymessage.has( "Message")) {
                            View layout = inflater.inflate(R.layout.custom_error_toast, null);
                            TextView message = (TextView) layout.findViewById(R.id.message);
                            message.setText(mymessage.getString("error"));
                            Toast toast = new Toast(getApplicationContext());
                            toast.setGravity(Gravity.TOP, 0, 0);
                            toast.setDuration(Toast.LENGTH_LONG);
                            toast.setView(layout);
                            toast.show();

                        } else {
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    Log.e("Success", new Gson().toJson(response.body()));

                } else {
                    response.errorBody(); // do something with that
                }
                progressBar2.setVisibility(View.GONE);
                submit.setVisibility(View.VISIBLE);
            }

            @Override
            public void onFailure(@NonNull Call<ResponseBody> call, @NonNull Throwable t) {
                Log.d("TAG", t.toString());
                progressBar2.setVisibility(View.GONE);
                submit.setVisibility(View.VISIBLE);
            }



        });
    }
}