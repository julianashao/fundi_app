package com.vodacom.fundiapp.activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.vodacom.fundiapp.R;
import com.vodacom.fundiapp.networking.FundFastaApi;
import com.vodacom.fundiapp.utils.FundiFastaUtils;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class ViewScheduled extends AppCompatActivity {
ProgressBar progressBar;
TextView address,sdate;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_scheduled);
        progressBar=findViewById(R.id.progressbar);
        Toolbar toolbar = findViewById(R.id.toolbar);
        address=findViewById(R.id.address);
        sdate=findViewById(R.id.date);
        address.setText(getIntent().getStringExtra("address"));
        String input = getIntent().getStringExtra("date");
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        DateFormat outputformat = new SimpleDateFormat("MMM dd,yyyy HH:mm aa");
        Date date = null;
        String output= null;
        try{
            date= df.parse(input);
            output = outputformat.format(date);
            sdate.setText(output);
        }catch(ParseException pe){
            pe.printStackTrace();
        }
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arrow) {
                ViewScheduled.this.onBackPressed();
            }
        });

        findViewById(R.id.start).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                updaterequestStatus();
            }
        });
        findViewById(R.id.direction).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(), Direction.class));
            }
        });
    }

    private void updaterequestStatus() {
        progressBar.setVisibility(View.VISIBLE);
        Retrofit retrofit = FundiFastaUtils.getRetrofit();
        FundFastaApi api = retrofit.create(FundFastaApi.class);
        String authorization=FundiFastaUtils.getBuyerAuthToken(getApplicationContext());
        String status="2";
        String request_id=getIntent().getStringExtra("id");
        api.updateFundiRequest(authorization,status,request_id

        ).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(@NonNull Call<ResponseBody> call, @NonNull Response<ResponseBody> response) {
                Log.e("Success", new Gson().toJson(response.body()));
                Log.e("erroie", String.valueOf(response));
                if (response.isSuccessful()) {
                    LayoutInflater inflater = getLayoutInflater();

                    try {
                        JSONObject mymessage = new JSONObject(response.body().string());
                        if (mymessage.getInt("status_code")==401){

                            View layout = inflater.inflate(R.layout.custom_error_toast, null);
                            TextView message = (TextView) layout.findViewById(R.id.message);
                            message.setText(mymessage.getString("error"));
                            Toast toast = new Toast(getApplicationContext());
                            toast.setGravity(Gravity.TOP, 0, 0);
                            toast.setDuration(Toast.LENGTH_LONG);
                            toast.setView(layout);
                            toast.show();
                        }else  if (mymessage.has( "Message")) {
                            View layout = inflater.inflate(R.layout.custom_success_toast,
                                    null);
                            TextView message = (TextView) layout.findViewById(R.id.message);
                            message.setText(mymessage.getString("Message"));
                            Toast toast = new Toast(getApplicationContext());
                            toast.setGravity(Gravity.TOP, 0, 0);
                            toast.setDuration(Toast.LENGTH_LONG);
                            toast.setView(layout);
                            toast.show();
                            startActivity(new Intent(getApplicationContext(), Home.class));
                            finish();
                        } else {
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    Log.e("Success", new Gson().toJson(response.body()));

                } else {
                    response.errorBody(); // do something with that
                }
                progressBar.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(@NonNull Call<ResponseBody> call, @NonNull Throwable t) {
                Log.d("TAG", t.toString());
                progressBar.setVisibility(View.GONE);
            }



        });
    }
}