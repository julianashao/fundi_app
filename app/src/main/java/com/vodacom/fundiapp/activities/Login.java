package com.vodacom.fundiapp.activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.vodacom.fundiapp.R;
import com.vodacom.fundiapp.networking.FundFastaApi;
import com.vodacom.fundiapp.utils.FundiFastaUtils;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class Login extends AppCompatActivity implements View.OnClickListener {

    EditText username, userpassword;
    Button loginButton;
    ProgressDialog ringProgressDialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        username = findViewById(R.id.username);
        userpassword = findViewById(R.id.password);
        loginButton = findViewById(R.id.signin);


        loginButton.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        String name = username.getText().toString();
        final String password = userpassword.getText().toString();

        if (TextUtils.isEmpty(name)) {
            Toast.makeText(getApplicationContext(), getString(R.string.enter_username), Toast.LENGTH_SHORT).show();
            return;
        }

        if (TextUtils.isEmpty(password)) {
            Toast.makeText(getApplicationContext(), getString(R.string.enter_password), Toast.LENGTH_SHORT).show();
            return;
        }

        loginUser(name,password);

    }


    private void loginUser(String name, String password) {
        //        progressBar.setVisibility(View.V/ISIBLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE, WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        Retrofit retrofit = FundiFastaUtils.getRetrofit();
        FundFastaApi api = retrofit.create(FundFastaApi.class);
        HashMap<String,String> hashMap=new HashMap<>();
        hashMap.put("username",name);
        hashMap.put("password",password);
        ringProgressDialog = ProgressDialog.show(Login.this, getString(R.string.please_wait), getString(R.string.almost_done), true);
        ringProgressDialog.setCancelable(false);
        api.userLogin(name,password

        ).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(@NonNull Call<ResponseBody> call, @NonNull Response<ResponseBody> response) {

                if (response.isSuccessful()) {
                    LayoutInflater inflater = getLayoutInflater();

                    try {
                        JSONObject mymessage = new JSONObject(response.body().string());
                        if (mymessage.getInt("status_code")==401){

                            View layout = inflater.inflate(R.layout.custom_error_toast, null);
                            TextView message = (TextView) layout.findViewById(R.id.message);
                            message.setText(mymessage.getString("Message"));
                            Toast toast = new Toast(getApplicationContext());
                            toast.setGravity(Gravity.TOP, 0, 0);
                            toast.setDuration(Toast.LENGTH_LONG);
                            toast.setView(layout);
                            toast.show();

                        }else  if (mymessage.has( "access_token")) {
                            View layout = inflater.inflate(R.layout.custom_success_toast,
                                    null);
                            TextView message = (TextView) layout.findViewById(R.id.message);
                            message.setText(mymessage.getString("Message"));
                            Toast toast = new Toast(getApplicationContext());
                            toast.setGravity(Gravity.TOP, 0, 0);
                            toast.setDuration(Toast.LENGTH_LONG);
                            toast.setView(layout);
                            toast.show();
                            String authToken = mymessage.getString("access_token");
                            String status = mymessage.getString("status");
                            String username = mymessage.getString("username");
                            storeAuthTokenPref(authToken,status,username);
                            startActivity(new Intent(getApplicationContext(), Home.class));
                            finish();
                        } else {
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    Log.e("Success", new Gson().toJson(response.body()));

                } else {
                    response.errorBody(); // do something with that
                }
                getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                if(ringProgressDialog != null)ringProgressDialog.dismiss();
            }

            @Override
            public void onFailure(@NonNull Call<ResponseBody> call, @NonNull Throwable t) {
                Log.d("TAG", t.toString());
                ringProgressDialog.dismiss();
                getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
            }



        });
    }


        private void storeAuthTokenPref(String authToken, String status, String username) {
            SharedPreferences sp = FundiFastaUtils.getSharedPreferences(this);
            sp.edit().putString("auth_token", "Bearer "+authToken).apply();
            sp.edit().putString("status", status).apply();
            sp.edit().putString("username", username).apply();
            sp.edit().putBoolean("user_logged_in", true).apply();
    }

    public void goreset(View view) {
        startActivity(new Intent(this, ResetPassword.class));
    }
}