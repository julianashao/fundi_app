package com.vodacom.fundiapp.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.PixelFormat;
import android.os.Bundle;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

import com.vodacom.fundiapp.R;
import com.vodacom.fundiapp.utils.FundiFastaUtils;

public class SplashScreen extends AppCompatActivity {


    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        Window window = getWindow();
        window.setFormat(PixelFormat.RGBA_8888);
    }
    /** Called when the activity is first created. */
    Thread splashTread;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
        StartAnimations();
    }
    private void StartAnimations() {
        Animation anim = AnimationUtils.loadAnimation(this, R.anim.alpha);
        anim.reset();
        ConstraintLayout l=(ConstraintLayout) findViewById(R.id.lin_lay);
        l.clearAnimation();
        l.startAnimation(anim);

        anim = AnimationUtils.loadAnimation(this, R.anim.translate);
        anim.reset();
        ImageView iv = (ImageView) findViewById(R.id.logo);
        iv.clearAnimation();
        iv.startAnimation(anim);

        splashTread = new Thread() {
            @Override
            public void run() {
                try {
                    int waited = 0;
                    // Splash screen pause time
                    while (waited < 3500) {
                        sleep(400);
                        waited += 400;
                    }

                    SharedPreferences sharedPreferences = FundiFastaUtils.getSharedPreferences(getApplicationContext());
                    Intent intent;

                    if (sharedPreferences.getBoolean("user_logged_in", false)) {
                        if (sharedPreferences.getBoolean("driver", true)) {
                            intent = new Intent(SplashScreen.this,
                                    Home.class);
                            startActivity(intent);
                            finish();
                        } else if (sharedPreferences.getBoolean("store", true)) {
                            intent = new Intent(SplashScreen.this,
                                    Login.class);
                            startActivity(intent);
                            finish();
                        }
                    }
                    else {
                        intent = new Intent(SplashScreen.this,
                                Login.class);
                        startActivity(intent);
                        finish();
                    }

                } catch (InterruptedException e) {
                    // do nothing
                } finally {
                    SplashScreen.this.finish();
                }

            }
        };
        splashTread.start();

    }
}