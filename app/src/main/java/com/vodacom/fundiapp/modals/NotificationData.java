package com.vodacom.fundiapp.modals;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class NotificationData {
    @SerializedName("notifications")
    public List<NotificationModal> notificationModalList;

}
