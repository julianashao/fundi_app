package com.vodacom.fundiapp.modals;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class DeliverableData {
    @SerializedName("data")
    public List<DeliverableModal> deliverableModalList;

}
