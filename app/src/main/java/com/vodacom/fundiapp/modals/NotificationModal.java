package com.vodacom.fundiapp.modals;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class NotificationModal {

    @SerializedName("id")
    @Expose
    private String id;

    @SerializedName("body")
    @Expose
    private String body;

    @SerializedName("action")
    @Expose
    private Integer action;

    @SerializedName("date")
    @Expose
    private String date;

    @SerializedName("request_id")
    @Expose
    private String request_id;



    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Integer getAction() {
        return action;
    }

    public void setAction(Integer action) {
        this.action = action;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getRequest_id() {
        return request_id;
    }

    public void setRequest_id(String request_id) {
        this.request_id = request_id;
    }
}
