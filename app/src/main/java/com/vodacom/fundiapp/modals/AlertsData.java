package com.vodacom.fundiapp.modals;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class AlertsData {
    @SerializedName("alerts")
    public List<AlertsModal> alertsModalList;

}
