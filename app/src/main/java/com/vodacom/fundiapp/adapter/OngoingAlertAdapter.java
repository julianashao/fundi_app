package com.vodacom.fundiapp.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.vodacom.fundiapp.R;
import com.vodacom.fundiapp.modals.AlertsModal;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;


public class OngoingAlertAdapter extends RecyclerView.Adapter<OngoingAlertAdapter.ViewHolder> {
    private List<AlertsModal> mDataset;
    Context context;
    OnItemClickListener mItemClickListener;
    public OngoingAlertAdapter(Context context, List<AlertsModal> myDataset) {

        this. context=context;
        mDataset = myDataset;
    }



    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        public TextView title,name,reqdate,startdate;
        ImageView photo;

        public ViewHolder(View v) {
            super(v);
            v.setOnClickListener(this);
            reqdate = v.findViewById(R.id.start);
            name = v.findViewById(R.id.name);
            title = v.findViewById(R.id.job);
            photo = v.findViewById(R.id.image);
            startdate = v.findViewById(R.id.workstartat);
        }

        @Override
        public void onClick(View v) {
            if (mItemClickListener != null) {
                mItemClickListener.onItemClick(v, getAdapterPosition());
            }
        }
    }

    @Override
    public OngoingAlertAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v =  LayoutInflater.from(parent.getContext())
                .inflate(R.layout.ongoing_task_item, parent, false);

        ViewHolder vh = new ViewHolder(v);
        return vh;
    }
    @Override
    public void onBindViewHolder(ViewHolder holder, int position)
    {
        holder.title.setText(mDataset.get(position).getTitle());
        holder.name.setText(mDataset.get(position).getName());
        String input = mDataset.get(position).getRequest_date();
        String input2 = mDataset.get(position).getStarted();
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        DateFormat outputformat = new SimpleDateFormat("MMM dd,yyyy HH:mm aa");
        Date date,date2 = null;
        String output,output2 = null;
        try{
            date= df.parse(input);
            date2= df.parse(input2);
            output = outputformat.format(date);
            output2 = outputformat.format(date2);
            holder.reqdate.setText(output);
            holder.startdate.setText(output2);
        }catch(ParseException pe){
            pe.printStackTrace();
        }
//        holder.reqdate.setText(mDataset.get(position).getRequest_date());
//        Integer status=mDataset.get(position).getStatus();
//        if(status==0){
//
//            holder.status.setText("Pending");
//        }
//        String url = mDataset.get(position).ge();
//        Glide.with(context)
//                .load(url)
//                .apply(RequestOptions.placeholderOf(R.drawable.logo).error(R.drawable.logo))
//                .into(holder.photo);

    }
    @Override
    public int getItemViewType(int position) {

        return super.getItemViewType(position);
    }

    @Override
    public int getItemCount() {
        return  mDataset.size();
    }

    public interface OnItemClickListener{
        void onItemClick(View view, int position);
    }

    public void setOnItemClickListener(final OnItemClickListener mItemClickListener) {
        this.mItemClickListener = mItemClickListener;
    }
}

