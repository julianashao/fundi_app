package com.vodacom.fundiapp.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.vodacom.fundiapp.R;
import com.vodacom.fundiapp.modals.DeliverableModal;

import java.util.ArrayList;
import java.util.List;


public class DeliverableAdapter extends RecyclerView.Adapter<DeliverableAdapter.ViewHolder> {
    private List<DeliverableModal> mDataset;
    Context context;
    OnItemClickListener mItemClickListener;
    public DeliverableAdapter(Context context, List<DeliverableModal> myDataset) {

        this. context=context;
        mDataset = myDataset;
    }



    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        public TextView title,price;
        public LinearLayout deliverableContainer;
        CheckBox checkBox;

        ImageView photo;

        public ViewHolder(View v) {
            super(v);
            v.setOnClickListener(this);
            deliverableContainer = v.findViewById(R.id.deliverable_container);
            title = v.findViewById(R.id.title);
            price = v.findViewById(R.id.price);
            checkBox = v.findViewById(R.id.checkbox);

        }

        @Override
        public void onClick(View v) {
            if (mItemClickListener != null) {
                mItemClickListener.onItemClick(v, getAdapterPosition());
            }
        }
    }



    @Override
    public DeliverableAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v =  LayoutInflater.from(parent.getContext())
                .inflate(R.layout.deleverable_item, parent, false);

        ViewHolder vh = new ViewHolder(v);
        return vh;
    }



    @Override
    public void onBindViewHolder(ViewHolder holder, int position)
    {
        holder.title.setText(mDataset.get(position).getName());
        holder.price.setText(mDataset.get(position).getPrice());
        holder.checkBox.setChecked(mDataset.get(position).isSelected());
//        holder.checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
//            @Override
//            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
//
//                mDataset.get(position).setSelected(isChecked);
//                Log.i("Checked",isChecked+"");
//            }
//        });

        holder.deliverableContainer.setOnClickListener(e->{
            if(holder.checkBox.isSelected()){
                mDataset.get(position).setSelected(false);
                Log.i("Checked",false+"");
            }else{
                mDataset.get(position).setSelected(true);
                Log.i("Checked",true+"");
            }
        });

    }
    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }

    @Override
    public int getItemCount() {
        return  mDataset.size();
    }

    public interface OnItemClickListener{
        void onItemClick(View view, int position);
    }

    public void setOnItemClickListener(final OnItemClickListener mItemClickListener) {
        this.mItemClickListener = mItemClickListener;
    }
}

