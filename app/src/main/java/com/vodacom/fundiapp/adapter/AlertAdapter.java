package com.vodacom.fundiapp.adapter;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.core.app.ActivityCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.vodacom.fundiapp.R;
import com.vodacom.fundiapp.modals.AlertsModal;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;



public class AlertAdapter extends RecyclerView.Adapter<AlertAdapter.ViewHolder> {
    private List<AlertsModal> mDataset;
    Context context;
    OnItemClickListener mItemClickListener;
    public AlertAdapter(Context context, List<AlertsModal> myDataset) {

        this. context=context;
        mDataset = myDataset;
    }



    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        public TextView title,name,reqdate,status,phone;
        ImageView photo;

        public ViewHolder(View v) {
            super(v);
            v.setOnClickListener(this);
            reqdate = v.findViewById(R.id.start);
            name = v.findViewById(R.id.name);
            title = v.findViewById(R.id.job);
            photo = v.findViewById(R.id.image);
            phone = v.findViewById(R.id.phone);

        }

        @Override
        public void onClick(View v) {
            if (mItemClickListener != null) {
                mItemClickListener.onItemClick(v, getAdapterPosition());
            }
        }
    }

    private void makecall(View view) {
        Intent callIntent = new Intent(Intent.ACTION_CALL);
        Uri call = Uri.parse("tel:0377778888");
        callIntent = new Intent(Intent.ACTION_CALL);
        callIntent.setData(call);

        try
        {
            if(Build.VERSION.SDK_INT > 22)
            {
                if (ActivityCompat.checkSelfPermission(context.getApplicationContext(), Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions((Activity) context, new String[]{Manifest.permission.CALL_PHONE}, 101);
                    return;
                }

                view.getContext().startActivity(callIntent);

            }
            else {
                view.getContext().startActivity(callIntent);

            }
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }



    }

    @Override
    public AlertAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v =  LayoutInflater.from(parent.getContext())
                .inflate(R.layout.task_item, parent, false);


        TextView phone_call=  v.findViewById(R.id.phone);
        phone_call.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String phone=mDataset.get(viewType).getPhone_number();
                pigaSimu(phone);

            }
        });

        ViewHolder vh = new ViewHolder(v);
        return vh;
    }


    private void pigaSimu(final String phoneNumber){

        context.startActivity(new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel",phoneNumber,null)));
    }


    @Override
    public void onBindViewHolder(ViewHolder holder, int position)
    {
        holder.title.setText(mDataset.get(position).getTitle());
        holder.name.setText(mDataset.get(position).getName());
        String input = mDataset.get(position).getRequest_date();
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        DateFormat outputformat = new SimpleDateFormat("MMM dd,yyyy HH:mm aa");
        Date date = null;
        String output = null;
        try{
            date= df.parse(input);
            output = outputformat.format(date);
            String mydate=output;
            holder.reqdate.setText(mydate);
        }catch(ParseException pe){
            pe.printStackTrace();
        }
//        Integer status=mDataset.get(position).getStatus();
//        if(status==0){
//
//            holder.status.setText("Pending");
//        }
//        String url = mDataset.get(position).ge();
//        Glide.with(context)
//                .load(url)
//                .apply(RequestOptions.placeholderOf(R.drawable.logo).error(R.drawable.logo))
//                .into(holder.photo);

    }
    @Override
    public int getItemViewType(int position) {

        return super.getItemViewType(position);
    }

    @Override
    public int getItemCount() {
        return  mDataset.size();
    }

    public interface OnItemClickListener{
        void onItemClick(View view, int position);
    }

    public void setOnItemClickListener(final OnItemClickListener mItemClickListener) {
        this.mItemClickListener = mItemClickListener;
    }
}

