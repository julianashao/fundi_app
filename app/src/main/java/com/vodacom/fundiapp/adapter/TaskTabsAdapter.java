package com.vodacom.fundiapp.adapter;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.vodacom.fundiapp.fragments.Completed;
import com.vodacom.fundiapp.fragments.Ongoing;
import com.vodacom.fundiapp.fragments.Pending;
import com.vodacom.fundiapp.fragments.Scheduled;


public class TaskTabsAdapter extends FragmentPagerAdapter {

    public TaskTabsAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return new Pending();
            case 1:
                return new Ongoing();
            case 2:
                return new Scheduled();
            case 3:
                return new Completed();
        }
        return null;
    }

    @Override
    public int getCount() {
        return 4;
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 0:
                return "PENDING";
            case 1:
                return "ONGOING";
            case 2:
                return "SCHEDULED";
            case 3:
                return "COMPLETED";
        }
        return "";
    }
}
